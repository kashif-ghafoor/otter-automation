import puppeteer from "puppeteer";
const proxyList = [
  "brd-customer-hl_a1111017-zone-zone1-ip-161.123.182.16:1n7wvlwkdd0j",
  "brd-customer-hl_a1111017-zone-zone1-ip-206.204.39.150:1n7wvlwkdd0j",
  "brd-customer-hl_a1111017-zone-zone1-ip-185.246.173.122:1n7wvlwkdd0j",
  "brd-customer-hl_a1111017-zone-zone1-ip-206.204.22.192:1n7wvlwkdd0j",
  "brd-customer-hl_a1111017-zone-zone1-ip-152.39.241.144:1n7wvlwkdd0j",
  "brd-customer-hl_a1111017-zone-zone1-ip-206.204.26.181:1n7wvlwkdd0j",
  "brd-customer-hl_a1111017-zone-zone1-ip-45.114.243.161:1n7wvlwkdd0j",
  "brd-customer-hl_a1111017-zone-zone1-ip-91.132.187.191:1n7wvlwkdd0j",
  "brd-customer-hl_a1111017-zone-zone1-ip-94.176.1.72:1n7wvlwkdd0j",
  "brd-customer-hl_a1111017-zone-zone1-ip-188.211.25.160:1n7wvlwkdd0j",
];

function parseProxy(proxyString: string) {
  const [username, password] = proxyString.split(":");
  return { username, password };
}

async function testProxyRotation() {
  let proxyIndex = 0;
  const maxRequestsPerIP = 2;
  let requestCount = 0;

  async function getPageWithProxy() {
    const { password, username } = parseProxy(proxyList[proxyIndex % proxyList.length]);
    const browser = await puppeteer.launch({
      defaultViewport: null,
      args: [`--proxy-server=zproxy.lum-superproxy.io:22225`],
    });
    const page = await browser.newPage();
    await page.authenticate({ username, password });
    console.log("applied following ip", username);
    return {
      page,
      browser,
    };
  }

  let { browser, page } = await getPageWithProxy();

  for (let i = 0; i < proxyList.length * maxRequestsPerIP; i++) {
    try {
      await page.goto("https://app.tryotter.com/login", { waitUntil: "networkidle2" });

      const heading = await page.evaluate(() => {
        return document.querySelector("input[type=email]")?.parentElement?.parentElement
          ?.textContent;
      });

      console.log(`${heading}`);
    } catch (error) {
      console.error(`Request ${i + 1} failed:`, error);
    }
    requestCount++;
    if (requestCount >= maxRequestsPerIP) {
      proxyIndex++;
      await browser.close();
      const { browser: newBrowser, page: newPage } = await getPageWithProxy();
      browser = newBrowser;
      page = newPage;
      requestCount = 0;
    }
  }

  await browser.close();
}

testProxyRotation();
