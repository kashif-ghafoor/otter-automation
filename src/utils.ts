import { Page, ElementHandle } from "puppeteer";

/**
 *
 * @param input - array strings where each string is a username containing dashes
 * @Description - splits the each string into an array and then combines elements of the array to create all possible usernames
 * @returns  - return an array of arrays where each array is a list of all possible usernames
 */
export function getUserNames(input: string[]) {
  return input.map((str) => {
    const splitString = str.split("-");
    const output = [splitString[0]];
    // add first and list element to output
    for (let i = 1; i < splitString.length; i++) {
      output.push(output[i - 1] + splitString[i]);
    }
    if (splitString.length > 2) {
      let counter = splitString.length - 1;
      while (isNumeric(splitString[counter]) && counter > 1) {
        counter--;
      }
      if (counter > 1) {
        // first word + city (city can be 2 words or one word )
        output.push(splitString[0] + splitString[counter]);
        output.push(splitString[0] + splitString[counter - 1] + splitString[counter]);

        // first word + 2nd word + city
        output.push(splitString[0] + splitString[1] + splitString[counter]);
        output.push(
          splitString[0] + splitString[1] + splitString[counter - 1] + splitString[counter]
        );
      }
    }
    return output;
  });
}

function isNumeric(value: string) {
  return /^\d+$/.test(value);
}

export const clearInput = async (page: Page, input: ElementHandle) => {
  await input.click({ clickCount: 3 });
  await page.keyboard.press("Backspace");
};

export const myClick = async (page: Page, selector: string) => {
  await page.evaluate((selector) => {
    const element: HTMLElement | null = document.querySelector(selector);
    element?.click();
  }, selector);
};

export const sleep = (ms: number) => {
  return new Promise((resolve) => setTimeout(resolve, ms));
};

export const getButtonElement = async (page: Page, selector: string) => {
  return await page.evaluate((selector) => {
    const element: HTMLButtonElement | null = document.querySelector(selector);
    return element;
  }, selector);
};

export const customLogout = async (page: Page) => {
  await page.evaluate(() => {
    window.localStorage.clear();
    window.location.reload();
    window.location.replace("/");
  });
};
