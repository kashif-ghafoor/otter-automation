//@ts-nocheck
import axios from "axios";
import cheerio from "cheerio";

/**
 * It scrapes https://free-proxy-list.net/ that lists free proxies, and returns a random proxy from the list
 * @returns A random proxy from the list of proxies.
 */
export async function randomProxyGenerator() {
  const proxies = await axios.get("https://free-proxy-list.net/");
  const $ = cheerio.load(proxies.data);
  const proxyList: string[] = [];
  $(".fpl-list tbody tr").each((i, elem) => {
    const proxy = $(elem).find("td").eq(0).text();
    const port = $(elem).find("td").eq(1).text();
    const https = $(elem).find("td").eq(6).text();
    if (https === "yes") {
      proxyList.push(`https://${proxy}:${port}`);
    } else {
      proxyList.push(`http://${proxy}:${port}`);
    }
  });
  const randomProxy = proxyList[Math.floor(Math.random() * proxyList.length)];
  return randomProxy;
}
