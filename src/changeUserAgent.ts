import puppeteer from "puppeteer-extra";
import { Page } from "puppeteer";

/**
 * @description change user agent of page
 * @param page puppeteer page
 */

export async function changeUserAgent(page: Page) {
  /**
   * if we just change the user agent string, server can detect bot by opening a new window
   * and checking the user agent string of the new window
   * so we need to change the user agent string of the new window too
   *
   * below script change the user agent string of the new window
   */
  await page.evaluateOnNewDocument(() => {
    let open = window.open;
    // removing headless from user agent string
    const ua = navigator.userAgent.replace(/HeadlessChrome/, "Chrome");
    window.open = (...args) => {
      let newPage = open(...args);
      Object.defineProperty(newPage?.navigator, "userAgent", {
        get: () => ua,
      });
      return newPage;
    };
    window.open.toString = () => "function open() { [native code] }";

    // detecting webdriver is the easiest way to detect bot
    // so we need to set web driver to false
    Object.defineProperty(navigator, "webdriver", {
      get: () => false,
    });
    return ua;
  });

  const ua = await page.evaluate(() => {
    return navigator.userAgent.replace(/HeadlessChrome/, "Chrome");
  });

  await page.setUserAgent(ua);
}
