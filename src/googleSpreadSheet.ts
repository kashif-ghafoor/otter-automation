import fs from "fs";
import { google } from "googleapis";
import { JWT } from "googleapis-common";

/**
 * This class is used to interact with the Google Sheet
 * it uses official google api v4
 * use it in this order.
 * const gs = new GoogleSheet()
 * await gs.authenticate();
 * Now you can get data
 *  await gs.getData()
 * to write data you first need to create output sheet
 * await gs.createSheet()
 * no you can write data
 * await gs.writeData()
 */

export class GoogleSheet {
  spreadSheetId: string;
  sheetId: number | null = null;
  auth: JWT | null = null;
  constructor() {
    const serviceAccount = JSON.parse(fs.readFileSync("service-account.json", "utf-8"));
    const spreadSheetId = serviceAccount.spreadSheetId;
    if (!spreadSheetId) {
      throw new Error("No spreadSheetId provided");
    }
    this.spreadSheetId = spreadSheetId;
  }

  async authenticate() {
    const content = fs.readFileSync("service-account.json", "utf-8");
    if (!content) {
      throw new Error("No service account file found");
    }
    const credentials = JSON.parse(content);

    const client = new google.auth.JWT(
      credentials.client_email,
      "service-account.json",
      credentials.private_key,
      ["https://www.googleapis.com/auth/spreadsheets"]
    );
    await client.authorize();
    this.auth = client;
  }

  async createSheet() {
    if (!this.auth) {
      throw new Error("No auth found");
    }
    const auth = this.auth;
    const sheets = google.sheets({ version: "v4", auth });

    const spreadsheet = await sheets.spreadsheets.get({
      spreadsheetId: this.spreadSheetId,
    });

    const sheetExists = spreadsheet.data.sheets?.some(
      (sheet) => sheet.properties?.title === "output sheet"
    );

    if (sheetExists) {
      const sheet = spreadsheet.data.sheets?.find(
        (sheet) => sheet.properties?.title === "output sheet"
      );
      this.sheetId = sheet?.properties?.sheetId ?? null;
      if (this.sheetId !== null) {
        console.log("output sheet already exists");
        return;
      }
    }

    const request = {
      spreadsheetId: this.spreadSheetId,
      resource: {
        requests: [
          {
            addSheet: {
              properties: {
                title: "output sheet",
              },
            },
          },
        ],
      },
    };
    var response = await sheets.spreadsheets.batchUpdate(request);

    const newSheet = response.data.replies?.[0].addSheet;
    this.sheetId = newSheet?.properties?.sheetId ?? null;
    if (this.sheetId == null) {
      throw new Error("Error creating sheet");
    }
    console.log("output sheet created");
  }

  async getData() {
    const auth = this.auth;
    if (!auth) {
      throw new Error("No auth found");
    }
    const sheets = google.sheets({ version: "v4", auth });
    const request = {
      spreadsheetId: this.spreadSheetId,
      range: "Sheet1", // The name of the sheet (e.g. "Sheet1")
    };

    // Use the sheets.spreadsheets.values.get method to retrieve the data from the Google Sheet
    const response = await sheets.spreadsheets.values.get(request);
    const rows = response.data.values;
    if (!rows || rows.length === 0) {
      console.log("No rows found on the sheet.");
      return [];
    }
    return rows;
  }

  async writeData(data: string[][]) {
    const auth = this.auth;
    if (!auth) {
      throw new Error("No auth found");
    }
    if (data[1][5] === "kitchen Printer On") {
      await this.addRow(auth, [data[0]]);
      await this.addRowWithColor(data[1]);
      return;
    }
    await this.addRow(auth, data);
  }

  private async addRowOld(auth: JWT, data: string[][]) {
    const sheets = google.sheets({ version: "v4", auth });
    const request = {
      spreadsheetId: this.spreadSheetId,
      range: "output sheet", // The name of the sheet (e.g. "Sheet1")
      valueInputOption: "USER_ENTERED",
      resource: {
        values: data,
      },
    };
    await sheets.spreadsheets.values.append(request);
  }

  private async addRow(auth: JWT, data: string[][]) {
    const sheets = google.sheets({ version: "v4", auth });

    // Find the last non-empty row in the "output sheet"
    const response = await sheets.spreadsheets.values.get({
      spreadsheetId: this.spreadSheetId,
      range: "output sheet",
    });
    const lastRow = response.data.values ? response.data.values.length : 0;

    // Append the new data to the last non-empty row
    const request = {
      spreadsheetId: this.spreadSheetId,
      range: `output sheet!A${lastRow + 1}`,
      valueInputOption: "USER_ENTERED",
      resource: {
        values: data,
      },
    };
    await sheets.spreadsheets.values.append(request);
  }

  private async addRowWithColor(row: string[]) {
    const auth = this.auth;
    if (!auth) {
      throw new Error("No auth found");
    }
    const sheets = google.sheets({ version: "v4", auth }); // Please use your authorization script.

    const spreadsheetId = this.spreadSheetId; // Please set your Spreadsheet ID.
    const backgroundColor = { red: 1, green: 0.8, blue: 0.7 }; // This is a sample background color. In this case, the red color is used.

    const values = row.map((e) => ({
      userEnteredValue: { stringValue: e },
      userEnteredFormat: { backgroundColor },
    }));
    await sheets.spreadsheets.batchUpdate({
      spreadsheetId,
      requestBody: {
        requests: [
          {
            appendCells: {
              rows: [{ values }],
              sheetId: this.sheetId,
              fields: "userEnteredValue,userEnteredFormat",
            },
          },
        ],
      },
    });
  }
}

// // for testing
// async () => {
//   const googleSheetHelper = new GoogleSheet();
//   await googleSheetHelper.authenticate();
//   await googleSheetHelper.createSheet();
//   const data = [
//     [
//       "dinnerbell-east-palo-alto",
//       "Dinnerbell",
//       "null,null,East Palo Alto,CA",
//       "Food Delivery Services",
//       "5",
//       "2",
//     ],
//     [
//       "",
//       "Dinner Bell Cafeteria & Bake Shop",
//       "6525 Lawndale St, Houston, TX 77023, USA",
//       "dinnerbell@orderotter.com",
//       "dinnerbell123",
//       "kitchen Printer On",
//       "$2,960.18",
//     ],
//   ];
//   await googleSheetHelper.writeData(data);
// };
