import { GoogleSpreadsheet, GoogleSpreadsheetWorksheet } from "google-spreadsheet";

export class GoogleSheetHelper {
  sheetId: string;
  client_email: string;
  private_key: string;
  doc: GoogleSpreadsheet;
  constructor() {
    const sheetId = process.env.GOOGLE_SHEET_ID;
    const client_email = process.env.GOOGLE_SERVICE_ACCOUNT_EMAIL;
    const private_key = process.env.GOOGLE_PRIVATE_KEY;
    if (!sheetId || !client_email || !private_key) {
      throw new Error("sheetId, email, or key is missing");
    }
    this.sheetId = sheetId;
    this.client_email = client_email;
    this.private_key = private_key;
    this.doc = new GoogleSpreadsheet(this.sheetId);
  }

  async auth() {
    await this.doc.useServiceAccountAuth({
      client_email: this.client_email,
      private_key: this.private_key,
    });
    await this.doc.loadInfo();
  }

  /**
   * will create new sheet if output sheet does not exist
   * @returns GoogleSpreadsheetWorksheet
   */
  async createSheet() {
    // get sheet with title output sheet
    let sheet = this.doc.sheetsByTitle["output sheet"];
    if (!sheet) {
      sheet = await this.doc.addSheet({ title: "output sheet" });
    }
    sheet.headerValues = [
      "placeholder",
      "store name",
      "address",
      "account",
      "password",
      "status",
      "reports",
    ];
    return sheet;
  }

  async getData(): Promise<string[][]> {
    const sheet = this.doc.sheetsByIndex[0];
    const rows = await sheet.getRows();
    const totalRows = [rows[0]._sheet.headerValues, ...rows.map((row) => row._rawData)];
    return totalRows;
  }

  async writeData(sheet: GoogleSpreadsheetWorksheet, rows: string[][]) {
    await sheet.addRows(rows);
  }
}
