import puppeteer from "puppeteer";

/**
 * @description
 * function gets the user agent of machine.
 * this is necessary to remove headless completely from userAgent string in headless browser.
 * @returns userAgent string
 */

export async function getUserAgent() {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  const userAgent = await page.evaluate(() => navigator.userAgent);
  await browser.close();
  return userAgent;
}
