import puppeteer from "puppeteer-extra";
import { Page } from "puppeteer";

/**
 * @description - this function will block image, font and stylesheet resources.
 * blocking resources
 * 1. will reduce the time to load the page.
 * 2. will reduce the bandwidth.
 * 3. make script execution faster.
 */

export async function blockResources(page: Page) {
  await page.setRequestInterception(true);

  page.on("request", (interceptedRequest) => {
    if (interceptedRequest.isInterceptResolutionHandled()) return;
    const sourceType = interceptedRequest.resourceType();

    if (sourceType == "image" || sourceType == "stylesheet" || sourceType == "font") {
      interceptedRequest.abort();
    } else {
      interceptedRequest.continue();
    }
  });
}
