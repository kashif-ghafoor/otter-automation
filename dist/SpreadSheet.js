"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GoogleSheetHelper = void 0;
const google_spreadsheet_1 = require("google-spreadsheet");
class GoogleSheetHelper {
    constructor() {
        const sheetId = process.env.GOOGLE_SHEET_ID;
        const client_email = process.env.GOOGLE_SERVICE_ACCOUNT_EMAIL;
        const private_key = process.env.GOOGLE_PRIVATE_KEY;
        if (!sheetId || !client_email || !private_key) {
            throw new Error("sheetId, email, or key is missing");
        }
        this.sheetId = sheetId;
        this.client_email = client_email;
        this.private_key = private_key;
        this.doc = new google_spreadsheet_1.GoogleSpreadsheet(this.sheetId);
    }
    auth() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.doc.useServiceAccountAuth({
                client_email: this.client_email,
                private_key: this.private_key,
            });
            yield this.doc.loadInfo();
        });
    }
    /**
     * will create new sheet if output sheet does not exist
     * @returns GoogleSpreadsheetWorksheet
     */
    createSheet() {
        return __awaiter(this, void 0, void 0, function* () {
            // get sheet with title output sheet
            let sheet = this.doc.sheetsByTitle["output sheet"];
            if (!sheet) {
                sheet = yield this.doc.addSheet({ title: "output sheet" });
            }
            sheet.headerValues = [
                "placeholder",
                "store name",
                "address",
                "account",
                "password",
                "status",
                "reports",
            ];
            return sheet;
        });
    }
    getData() {
        return __awaiter(this, void 0, void 0, function* () {
            const sheet = this.doc.sheetsByIndex[0];
            const rows = yield sheet.getRows();
            const totalRows = [rows[0]._sheet.headerValues, ...rows.map((row) => row._rawData)];
            return totalRows;
        });
    }
    writeData(sheet, rows) {
        return __awaiter(this, void 0, void 0, function* () {
            yield sheet.addRows(rows);
        });
    }
}
exports.GoogleSheetHelper = GoogleSheetHelper;
