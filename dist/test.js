"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const puppeteer_1 = __importDefault(require("puppeteer"));
const proxyList = [
    "brd-customer-hl_a1111017-zone-zone1-ip-161.123.182.16:1n7wvlwkdd0j",
    "brd-customer-hl_a1111017-zone-zone1-ip-206.204.39.150:1n7wvlwkdd0j",
    "brd-customer-hl_a1111017-zone-zone1-ip-185.246.173.122:1n7wvlwkdd0j",
    "brd-customer-hl_a1111017-zone-zone1-ip-206.204.22.192:1n7wvlwkdd0j",
    "brd-customer-hl_a1111017-zone-zone1-ip-152.39.241.144:1n7wvlwkdd0j",
    "brd-customer-hl_a1111017-zone-zone1-ip-206.204.26.181:1n7wvlwkdd0j",
    "brd-customer-hl_a1111017-zone-zone1-ip-45.114.243.161:1n7wvlwkdd0j",
    "brd-customer-hl_a1111017-zone-zone1-ip-91.132.187.191:1n7wvlwkdd0j",
    "brd-customer-hl_a1111017-zone-zone1-ip-94.176.1.72:1n7wvlwkdd0j",
    "brd-customer-hl_a1111017-zone-zone1-ip-188.211.25.160:1n7wvlwkdd0j",
];
function parseProxy(proxyString) {
    const [username, password] = proxyString.split(":");
    return { username, password };
}
function testProxyRotation() {
    return __awaiter(this, void 0, void 0, function* () {
        let proxyIndex = 0;
        const maxRequestsPerIP = 2;
        let requestCount = 0;
        function getPageWithProxy() {
            return __awaiter(this, void 0, void 0, function* () {
                const { password, username } = parseProxy(proxyList[proxyIndex % proxyList.length]);
                const browser = yield puppeteer_1.default.launch({
                    defaultViewport: null,
                    args: [`--proxy-server=zproxy.lum-superproxy.io:22225`],
                });
                const page = yield browser.newPage();
                yield page.authenticate({ username, password });
                console.log("applied following ip", username);
                return {
                    page,
                    browser,
                };
            });
        }
        let { browser, page } = yield getPageWithProxy();
        for (let i = 0; i < proxyList.length * maxRequestsPerIP; i++) {
            try {
                yield page.goto("https://app.tryotter.com/login", { waitUntil: "networkidle2" });
                const heading = yield page.evaluate(() => {
                    var _a, _b, _c;
                    return (_c = (_b = (_a = document.querySelector("input[type=email]")) === null || _a === void 0 ? void 0 : _a.parentElement) === null || _b === void 0 ? void 0 : _b.parentElement) === null || _c === void 0 ? void 0 : _c.textContent;
                });
                console.log(`${heading}`);
            }
            catch (error) {
                console.error(`Request ${i + 1} failed:`, error);
            }
            requestCount++;
            if (requestCount >= maxRequestsPerIP) {
                proxyIndex++;
                yield browser.close();
                const { browser: newBrowser, page: newPage } = yield getPageWithProxy();
                browser = newBrowser;
                page = newPage;
                requestCount = 0;
            }
        }
        yield browser.close();
    });
}
testProxyRotation();
