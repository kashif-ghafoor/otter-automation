"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.randomProxyGenerator = void 0;
//@ts-nocheck
const axios_1 = __importDefault(require("axios"));
const cheerio_1 = __importDefault(require("cheerio"));
/**
 * It scrapes https://free-proxy-list.net/ that lists free proxies, and returns a random proxy from the list
 * @returns A random proxy from the list of proxies.
 */
function randomProxyGenerator() {
    return __awaiter(this, void 0, void 0, function* () {
        const proxies = yield axios_1.default.get("https://free-proxy-list.net/");
        const $ = cheerio_1.default.load(proxies.data);
        const proxyList = [];
        $(".fpl-list tbody tr").each((i, elem) => {
            const proxy = $(elem).find("td").eq(0).text();
            const port = $(elem).find("td").eq(1).text();
            const https = $(elem).find("td").eq(6).text();
            if (https === "yes") {
                proxyList.push(`https://${proxy}:${port}`);
            }
            else {
                proxyList.push(`http://${proxy}:${port}`);
            }
        });
        const randomProxy = proxyList[Math.floor(Math.random() * proxyList.length)];
        return randomProxy;
    });
}
exports.randomProxyGenerator = randomProxyGenerator;
