"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GoogleSheet = void 0;
const fs_1 = __importDefault(require("fs"));
const googleapis_1 = require("googleapis");
/**
 * This class is used to interact with the Google Sheet
 * it uses official google api v4
 * use it in this order.
 * const gs = new GoogleSheet()
 * await gs.authenticate();
 * Now you can get data
 *  await gs.getData()
 * to write data you first need to create output sheet
 * await gs.createSheet()
 * no you can write data
 * await gs.writeData()
 */
class GoogleSheet {
    constructor() {
        this.sheetId = null;
        this.auth = null;
        const serviceAccount = JSON.parse(fs_1.default.readFileSync("service-account.json", "utf-8"));
        const spreadSheetId = serviceAccount.spreadSheetId;
        if (!spreadSheetId) {
            throw new Error("No spreadSheetId provided");
        }
        this.spreadSheetId = spreadSheetId;
    }
    authenticate() {
        return __awaiter(this, void 0, void 0, function* () {
            const content = fs_1.default.readFileSync("service-account.json", "utf-8");
            if (!content) {
                throw new Error("No service account file found");
            }
            const credentials = JSON.parse(content);
            const client = new googleapis_1.google.auth.JWT(credentials.client_email, "service-account.json", credentials.private_key, ["https://www.googleapis.com/auth/spreadsheets"]);
            yield client.authorize();
            this.auth = client;
        });
    }
    createSheet() {
        var _a, _b, _c, _d, _e, _f, _g;
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.auth) {
                throw new Error("No auth found");
            }
            const auth = this.auth;
            const sheets = googleapis_1.google.sheets({ version: "v4", auth });
            const spreadsheet = yield sheets.spreadsheets.get({
                spreadsheetId: this.spreadSheetId,
            });
            const sheetExists = (_a = spreadsheet.data.sheets) === null || _a === void 0 ? void 0 : _a.some((sheet) => { var _a; return ((_a = sheet.properties) === null || _a === void 0 ? void 0 : _a.title) === "output sheet"; });
            if (sheetExists) {
                const sheet = (_b = spreadsheet.data.sheets) === null || _b === void 0 ? void 0 : _b.find((sheet) => { var _a; return ((_a = sheet.properties) === null || _a === void 0 ? void 0 : _a.title) === "output sheet"; });
                this.sheetId = (_d = (_c = sheet === null || sheet === void 0 ? void 0 : sheet.properties) === null || _c === void 0 ? void 0 : _c.sheetId) !== null && _d !== void 0 ? _d : null;
                if (this.sheetId !== null) {
                    console.log("output sheet already exists");
                    return;
                }
            }
            const request = {
                spreadsheetId: this.spreadSheetId,
                resource: {
                    requests: [
                        {
                            addSheet: {
                                properties: {
                                    title: "output sheet",
                                },
                            },
                        },
                    ],
                },
            };
            var response = yield sheets.spreadsheets.batchUpdate(request);
            const newSheet = (_e = response.data.replies) === null || _e === void 0 ? void 0 : _e[0].addSheet;
            this.sheetId = (_g = (_f = newSheet === null || newSheet === void 0 ? void 0 : newSheet.properties) === null || _f === void 0 ? void 0 : _f.sheetId) !== null && _g !== void 0 ? _g : null;
            if (this.sheetId == null) {
                throw new Error("Error creating sheet");
            }
            console.log("output sheet created");
        });
    }
    getData() {
        return __awaiter(this, void 0, void 0, function* () {
            const auth = this.auth;
            if (!auth) {
                throw new Error("No auth found");
            }
            const sheets = googleapis_1.google.sheets({ version: "v4", auth });
            const request = {
                spreadsheetId: this.spreadSheetId,
                range: "Sheet1", // The name of the sheet (e.g. "Sheet1")
            };
            // Use the sheets.spreadsheets.values.get method to retrieve the data from the Google Sheet
            const response = yield sheets.spreadsheets.values.get(request);
            const rows = response.data.values;
            if (!rows || rows.length === 0) {
                console.log("No rows found on the sheet.");
                return [];
            }
            return rows;
        });
    }
    writeData(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const auth = this.auth;
            if (!auth) {
                throw new Error("No auth found");
            }
            if (data[1][5] === "kitchen Printer On") {
                yield this.addRow(auth, [data[0]]);
                yield this.addRowWithColor(data[1]);
                return;
            }
            yield this.addRow(auth, data);
        });
    }
    addRowOld(auth, data) {
        return __awaiter(this, void 0, void 0, function* () {
            const sheets = googleapis_1.google.sheets({ version: "v4", auth });
            const request = {
                spreadsheetId: this.spreadSheetId,
                range: "output sheet",
                valueInputOption: "USER_ENTERED",
                resource: {
                    values: data,
                },
            };
            yield sheets.spreadsheets.values.append(request);
        });
    }
    addRow(auth, data) {
        return __awaiter(this, void 0, void 0, function* () {
            const sheets = googleapis_1.google.sheets({ version: "v4", auth });
            // Find the last non-empty row in the "output sheet"
            const response = yield sheets.spreadsheets.values.get({
                spreadsheetId: this.spreadSheetId,
                range: "output sheet",
            });
            const lastRow = response.data.values ? response.data.values.length : 0;
            // Append the new data to the last non-empty row
            const request = {
                spreadsheetId: this.spreadSheetId,
                range: `output sheet!A${lastRow + 1}`,
                valueInputOption: "USER_ENTERED",
                resource: {
                    values: data,
                },
            };
            yield sheets.spreadsheets.values.append(request);
        });
    }
    addRowWithColor(row) {
        return __awaiter(this, void 0, void 0, function* () {
            const auth = this.auth;
            if (!auth) {
                throw new Error("No auth found");
            }
            const sheets = googleapis_1.google.sheets({ version: "v4", auth }); // Please use your authorization script.
            const spreadsheetId = this.spreadSheetId; // Please set your Spreadsheet ID.
            const backgroundColor = { red: 1, green: 0.8, blue: 0.7 }; // This is a sample background color. In this case, the red color is used.
            const values = row.map((e) => ({
                userEnteredValue: { stringValue: e },
                userEnteredFormat: { backgroundColor },
            }));
            yield sheets.spreadsheets.batchUpdate({
                spreadsheetId,
                requestBody: {
                    requests: [
                        {
                            appendCells: {
                                rows: [{ values }],
                                sheetId: this.sheetId,
                                fields: "userEnteredValue,userEnteredFormat",
                            },
                        },
                    ],
                },
            });
        });
    }
}
exports.GoogleSheet = GoogleSheet;
// // for testing
// async () => {
//   const googleSheetHelper = new GoogleSheet();
//   await googleSheetHelper.authenticate();
//   await googleSheetHelper.createSheet();
//   const data = [
//     [
//       "dinnerbell-east-palo-alto",
//       "Dinnerbell",
//       "null,null,East Palo Alto,CA",
//       "Food Delivery Services",
//       "5",
//       "2",
//     ],
//     [
//       "",
//       "Dinner Bell Cafeteria & Bake Shop",
//       "6525 Lawndale St, Houston, TX 77023, USA",
//       "dinnerbell@orderotter.com",
//       "dinnerbell123",
//       "kitchen Printer On",
//       "$2,960.18",
//     ],
//   ];
//   await googleSheetHelper.writeData(data);
// };
