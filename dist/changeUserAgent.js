"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.changeUserAgent = void 0;
/**
 * @description change user agent of page
 * @param page puppeteer page
 */
function changeUserAgent(page) {
    return __awaiter(this, void 0, void 0, function* () {
        /**
         * if we just change the user agent string, server can detect bot by opening a new window
         * and checking the user agent string of the new window
         * so we need to change the user agent string of the new window too
         *
         * below script change the user agent string of the new window
         */
        yield page.evaluateOnNewDocument(() => {
            let open = window.open;
            // removing headless from user agent string
            const ua = navigator.userAgent.replace(/HeadlessChrome/, "Chrome");
            window.open = (...args) => {
                let newPage = open(...args);
                Object.defineProperty(newPage === null || newPage === void 0 ? void 0 : newPage.navigator, "userAgent", {
                    get: () => ua,
                });
                return newPage;
            };
            window.open.toString = () => "function open() { [native code] }";
            // detecting webdriver is the easiest way to detect bot
            // so we need to set web driver to false
            Object.defineProperty(navigator, "webdriver", {
                get: () => false,
            });
            return ua;
        });
        const ua = yield page.evaluate(() => {
            return navigator.userAgent.replace(/HeadlessChrome/, "Chrome");
        });
        yield page.setUserAgent(ua);
    });
}
exports.changeUserAgent = changeUserAgent;
