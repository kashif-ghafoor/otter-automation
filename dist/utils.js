"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.customLogout = exports.getButtonElement = exports.sleep = exports.myClick = exports.clearInput = exports.getUserNames = void 0;
/**
 *
 * @param input - array strings where each string is a username containing dashes
 * @Description - splits the each string into an array and then combines elements of the array to create all possible usernames
 * @returns  - return an array of arrays where each array is a list of all possible usernames
 */
function getUserNames(input) {
    return input.map((str) => {
        const splitString = str.split("-");
        const output = [splitString[0]];
        // add first and list element to output
        for (let i = 1; i < splitString.length; i++) {
            output.push(output[i - 1] + splitString[i]);
        }
        if (splitString.length > 2) {
            let counter = splitString.length - 1;
            while (isNumeric(splitString[counter]) && counter > 1) {
                counter--;
            }
            if (counter > 1) {
                // first word + city (city can be 2 words or one word )
                output.push(splitString[0] + splitString[counter]);
                output.push(splitString[0] + splitString[counter - 1] + splitString[counter]);
                // first word + 2nd word + city
                output.push(splitString[0] + splitString[1] + splitString[counter]);
                output.push(splitString[0] + splitString[1] + splitString[counter - 1] + splitString[counter]);
            }
        }
        return output;
    });
}
exports.getUserNames = getUserNames;
function isNumeric(value) {
    return /^\d+$/.test(value);
}
const clearInput = (page, input) => __awaiter(void 0, void 0, void 0, function* () {
    yield input.click({ clickCount: 3 });
    yield page.keyboard.press("Backspace");
});
exports.clearInput = clearInput;
const myClick = (page, selector) => __awaiter(void 0, void 0, void 0, function* () {
    yield page.evaluate((selector) => {
        const element = document.querySelector(selector);
        element === null || element === void 0 ? void 0 : element.click();
    }, selector);
});
exports.myClick = myClick;
const sleep = (ms) => {
    return new Promise((resolve) => setTimeout(resolve, ms));
};
exports.sleep = sleep;
const getButtonElement = (page, selector) => __awaiter(void 0, void 0, void 0, function* () {
    return yield page.evaluate((selector) => {
        const element = document.querySelector(selector);
        return element;
    }, selector);
});
exports.getButtonElement = getButtonElement;
const customLogout = (page) => __awaiter(void 0, void 0, void 0, function* () {
    yield page.evaluate(() => {
        window.localStorage.clear();
        window.location.reload();
        window.location.replace("/");
    });
});
exports.customLogout = customLogout;
