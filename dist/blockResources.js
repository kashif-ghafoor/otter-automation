"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.blockResources = void 0;
/**
 * @description - this function will block image, font and stylesheet resources.
 * blocking resources
 * 1. will reduce the time to load the page.
 * 2. will reduce the bandwidth.
 * 3. make script execution faster.
 */
function blockResources(page) {
    return __awaiter(this, void 0, void 0, function* () {
        yield page.setRequestInterception(true);
        page.on("request", (interceptedRequest) => {
            if (interceptedRequest.isInterceptResolutionHandled())
                return;
            const sourceType = interceptedRequest.resourceType();
            if (sourceType == "image" || sourceType == "stylesheet" || sourceType == "font") {
                interceptedRequest.abort();
            }
            else {
                interceptedRequest.continue();
            }
        });
    });
}
exports.blockResources = blockResources;
