
# otter automation

this bot tries to login into otter site with different email and password combinations and if bot is able to login then it extracts data from site.

- store name
- address
- account
- password
- status
- reports





## Run Locally

To run the script successfully you need 2 things:

-  
- service-account.json
- google sheet id. 
To get json file (follow [this](https://theoephraim.github.io/node-google-spreadsheet/#/getting-started/authentication) documentation for service account).  
google sheet id is random string in url of google sheet where you want to store data.

Note: you have to explicitly give google sheet access to the email you created.

Clone the project

```bash
  git clone https://github.com/kashif-ghafoor/otter-automation.git
```
Go to the project directory

```bash
  cd otter-automation                    
```

Install dependencies

```bash
  npm install
```

open the otter-automation folder in file explorer. move your json file that you downloaded to this folder and rename it to `service-account.json`. 

Now open `service-account.json`. it's an object. inside this object add new property named `spreadSheetId` and assign google sheet id as value to this property.


Start the script

```bash
  npm run start
```

wait for the script to complete you will see data in your google sheets.
## Run on google compute engine
- install nodejs:
```
sudo apt-get update
```

```
curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash - && sudo apt-get install -y nodejs
```

install git

```
sudo apt-get install git

```

install dependencies (for puppeteer)
```
sudo apt-get install ca-certificates fonts-liberation libappindicator3-1 libasound2 libatk-bridge2.0-0 libatk1.0-0 libc6 libcairo2 libcups2 libdbus-1-3 libexpat1 libfontconfig1 libgbm1 libgcc1 libglib2.0-0 libgtk-3-0 libnspr4 libnss3 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 lsb-release wget xdg-utils

```

clone repo

```
git clone https://github.com/kashif-ghafoor/otter-automation.git
```
you have to create personal access token.

upload `service-account.json` to virtual machine. 

Note: `service-account.json` file should contain spreadSheetId 

```
cd otter-automation                    
```

move `service-account.json` file to project folder

```
mv ~/service-account.json .
```
install dependencies
```
npm i
```
run bot
```
npm run start
```
